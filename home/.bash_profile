if [ "$(tty)" == "/dev/tty1" ] ; then
	su -c "timedatectl set-ntp true && cryptsetup open /dev/sdc1 sdc1.crypt --key-file /home/alice/.config/keyfiles/sdc1.keyfile && cryptsetup open /dev/sdd2 sdd2.crypt --key-file /home/alice/.config/keyfiles/sdd2.keyfile && mount /dev/mapper/sdc1.crypt /crypt/downloads && mount /dev/mapper/sdd2.crypt /crypt/data"
	startx && exit
fi
source ~/.bashrc
