set number
syntax on

:set filetype=i3config

:set tabstop=4

aug i3config_ft_detection
  au!
  au BufNewFile,BufRead ~/.config/i3/config set filetype=i3config
aug end
