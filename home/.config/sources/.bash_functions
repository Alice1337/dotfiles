function crypt {
	local mounted=$(lsblk | grep crypt_part)
	if [[ -z "$mounted" ]]
	then
		echo mounting
		user=$(whoami)
		sudo cryptsetup open /dev/sde1 crypt_part
		sudo mount /dev/mapper/crypt_part /mnt/crypt
		sudo chown -R $user:$user /mnt/crypt/
	elif [ -n "$mounted" ]
	then
		echo unmounting
		sudo umount /mnt/crypt
		sudo cryptsetup close crypt_part
	fi
}

function rc {
        if [ -z "$1" ]
        then
                vim ~/.bashrc
                source ~/.bashrc
        elif [ "$1" == "a" ]
        then
                vim ~/.config/sources/.bash_aliases
                source ~/.bashrc
        elif [ "$1" == "f" ]
        then
                vim ~/.config/sources/.bash_functions
                source ~/.bashrc
        elif [ "$1" == "s" ]
        then
                source ~/.bashrc
        fi
}

function pid {
        local process="$1"
        echo $(pidof "$process")
}

function mkcd {
        mkdir -v "$1"
        cd "$1"
}

function uzip {
        local extract="$1"
        7z x "$extract" -o"*"
}

function search {
        find ./"$2" -iname "*$1*"
}

function newsh {
        local script="$1"
        local pwd=$(pwd)
        echo -e '#!/bin/bash\n\n' > "$pwd"/"$script"
        chmod +x "$pwd"/"$script"
        vim +3 "$pwd"/"$script"
}