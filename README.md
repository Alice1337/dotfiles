# dotfiles
home => user specific<br>
root => /root user<br>
usr/share => icons and themes<br>


## OS and packages
An Arch-install-script can be found in<br>
<div class="highlight highlight-source-shell"><pre>home/.config/scripts/archinstaller</pre></div>
<br>
packages needed for a working GUI:<br>
<div class="highlight highlight-source-shell"><pre>xorg-server<br>xinit<br>lxterminal<br>i3-gaps<br>dmenu<br>polybar<br>dunst<br>picom<br>feh<br>thunar<br>lxappearance</pre></div>
