#!/bin/sh

# Terminate already running bar instances
killall -9 polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar --config=~/.config/polybar/config DPR
polybar --config=~/.config/polybar/config DPL

echo "Bars launched..."
