alias !='sudo $(fc -ln -1)'
alias cd.='cd ..'
alias cd..='cd ../..'
alias cd...='cd ../../..'
alias cp='cp -iv'
alias cpr='cp -ri'
alias dir='dir --color --group-directories-first'
alias egrep='egrep --color=always'
alias fgrep='fgrep --color=always'
alias grep='grep --color=always'
alias h='reset'
alias hh='cd && reset'
alias l='ls --color --group-directories-first'
alias la='ls -a --color --group-directories-first'
alias ll='ls -l --color --group-directories-first'
alias lla='ls -la --color --group-directories-first'
alias neofetch='clear && neofetch --ascii_colors 68 63 --colors 63 68 6 63 6 6 --os_arch off'
alias pacbloat='sudo pacman -Rsn $(pacman -Qqdt)'
alias paczen='trizen --noedit -r'
alias rm='rm -rI'
alias rmf='rm -rIf'
alias rmv='rm -rIv'
alias rscp='rsync -r --info=progress2'
alias rsa='rsync -a --info=progress2'
alias ss='sudo systemctl'
alias top='sudo htop -t'
alias trz='trizen'
alias trizen='trizen --noedit'
alias utar='tar -xvf'
alias vdir='vdir --color --group-directories-first'
alias xcl="xprop | grep CLAS
