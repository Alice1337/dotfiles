## Variablen
PS1=' \[\033[1;32m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h\[\033[0;0m\] \w \[\033[0;31m\]\$\[\033[0;0m\] '
sources=/root/.config/sources/
red='\e[31m'
norm='\e[0m'


## Exports
export PATH={$PATH}:~/.config/scripts
export LS_COLORS='ow=01;37:di=01;34:fi=36:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=01;32:*.rpm=90'
export VISUAL=vim
export EDITOR=vim
export TERM=xterm-256color
export BROWSER=brave


## Sourcefiles
if [ -f "$sources".bash_aliases ]
then
	source $sources.bash_aliases
else
	echo -e ""$red""$sources".bash_aliases konnte nicht gesourced werden"$norm""
fi

if [ -f "$sources".bash_functions ]
then
	source $sources.bash_functions
else
	echo -e ""$red""$sources".bash_functions konnte nicht gesourced werden"$norm""
fi


## Optionen
set -o vi
bind 'set show-all-if-ambiguous on'
# bind 'TAB:menu-complete'
